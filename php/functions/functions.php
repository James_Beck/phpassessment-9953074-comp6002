<?php 
//Define the timezone
date_default_timezone_set('Pacific/Auckland');

//Define values for the database - I chose to do this within the same file
define("DBHOST", "localhost");
define("DBUSER", "root");
define("DBPASS", "root");
define("DBNAME", "comp6002_17a_assn2");

//function that creates a connection to the database - Useful for other functions
function connection() {
    
    $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    
    return $conn;
}

//puts data from the database, filtered by relevant title to the index page
function get_all_items($received) {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_consumables WHERE CATEGORY = '$received'";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "name" => $row['NAME'],
            "price" => $row['PRICE'],
            "category" => $row['CATEGORY']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//Shows the relevant data for a page - with additions for administrator login
function show_all_items($data, $page) {
    
    $array = json_decode($data, True);    
    $output = "";

    if (count($array) > 0 ) {        
        for ($i = 0; $i < count($array); $i++) {            
            if ($page == "index") {
                $output .= "<tr><td>".$array[$i]['name']."</td><td>".number_format($array[$i]['price'], 2)."</td></tr>";
            }            
            if ($page == "admin") {
                $output .= "<tr><td>".$array[$i]['name']."</td><td>".number_format($array[$i]['price'], 2)."</td><td><a class=\"btn btn-danger\" style=\"padding: 0\" href=\"delete.php?id=".$array[$i]['id']."\">delete</a></td><td><a class=\"btn btn-info\" style=\"padding: 0\" href=\"edit.php?id=".$array[$i]['id']."\">edit</a></td></tr>";            }   
        } 
    }
    else {
        $output .= "<tr><td colspan='5'>No Data Available</td></tr>";        
        return $output;
    }

    if ($page == "admin") {
        $output .= "<tr><td><a class=\"btn btn-success\" href=\"add.php\">Add to menu</a></td><td><form method=\"post\"><input type=\"submit\" class='btn btn-warning' name=\"reset\" value='Truncate'></input></form></td></tr>";
    }
    return $output;
}

//Edit Data
function loadData($id) {

    $db = connection();
    $sql = "SELECT * FROM tbl_consumables WHERE ID = $id";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "name" => $row['NAME'],
            "price" => $row['PRICE'],
            "category" => $row['CATEGORY']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;        
}

//Fill in data for forms

function displayID()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['id'];
}

function displayName()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['name'];
}

function displayPrice()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['price'];
}

//Saving Changes
function editRecord() {

    if(isset($_POST['updateItem']))
    {
        $db = connection();

        $name = $db->real_escape_string($_POST['name']);
        $price = $db->real_escape_string($_POST['price']);
        $category = $db->real_escape_string($_POST['cat']);
        $id = $db->real_escape_string($_POST['id']);

        $sql = "UPDATE tbl_consumables SET NAME='".$name."', PRICE='".$price."', CATEGORY='".$category."' WHERE ID = ".$id."";

        $result = $db->query($sql);

        if ($result == 1) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            return "<br><br>An Error has occured";
            exit();
        }
    }  
}

//Add an item to the database
function addRecord() {

    if(isset($_POST['addItem']))
    {
        $db = connection();

        $product = $db->real_escape_string($_POST['name']);
        $price = $db->real_escape_string($_POST['price']);
        $choice = $db->real_escape_string($_POST['cat']);

        $stmt = $db->prepare("INSERT INTO tbl_consumables (`NAME`, `PRICE`, `CATEGORY`) VALUES (?, ?, ?)");
        $stmt->bind_param("sss", $product, $price, $choice);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result > 0) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

//Deletes a value in the table
function removeSingleRecord() {

    if(isset($_POST['removeRecord']))
    {
        $db = connection();

        $id = $db->real_escape_string($_POST['id']);    

        $stmt = $db->prepare("DELETE FROM tbl_consumables WHERE ID = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result == 1) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

//Resets data
function resetData() {

    if(isset($_POST['reset']))
    {
        $db = connection();

        $sql = "TRUNCATE TABLE tbl_consumables; ";
        $sql .= "INSERT INTO `tbl_consumables` (`ID`, `NAME`, `PRICE`, `CATEGORY`) VALUES";
        $sql .= " (2, 'Latte', 2.75, 'Drinks'),";
        $sql .= " (3, 'Chocolate Cake', 3.5, 'SIDES'),";
        $sql .= " (4, 'Hot Chips', 3, 'SIDES'),";
        $sql .= " (6, 'Something', 3, 'Drinks');";
        
        $select = $db->query($sql);

        $db->close();

        logout();
    }
}

//Admin login
function loginAdmin() {

    if(isset($_POST['login']))
    {
        $db = connection();

        $user = $db->real_escape_string($_POST['username']);
        $pass = $db->real_escape_string($_POST['password']);

        $sql = "SELECT * FROM tbl_admin WHERE NAME = '$user' && PASSWRD = '$pass'";
        $arr = [];

        $result = $db->query($sql);

        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "user" => $row['NAME'],
                "pass" => $row['PASSWRD']
            );
        }

        $result->free();
        $db->close();

        if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass']))
        {
            $_SESSION['login'] = TRUE;
            redirect("admin/index.php");
        }
        else
        {
            echo "<h1 class='removeSure'>Your login details are incorrect.</h1>";
            redirect("index.php");
        }
    }
}

//Logout when logged in as administrator
function logout()
{
    if(isset($_POST['logout']))
    {
        session_start();

        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();

        redirect("../index.php");
    }
}

//Redirect to other pages with Javascript
function redirect($location)
{
    $URL = $location;
    echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
    echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
    exit();
}

?>